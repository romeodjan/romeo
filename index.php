<!DOCTYPE>
<html>
<div id="dvLoading"></div>

<?php 
//Nom du site web
$site="Hôtel Carrefour Séguela";

//Nombre de photo par slide
$nbre_img=5 ;

//Téléphone
$tel="+225 32 74 04 69";

//Fax
$fax="+225 32 74 04 69";

//Fax
$email="info@hotelcarrefour.ci";
?>

<?php 
// Compteur de visite par session 
include('include/compteur.php'); 
?>

<head>

<?php include("include/meta.php"); ?>  
<?php include("include/head.php"); ?>
   <!-- contenu dynamique -->     
   
<?php

if(isset($_GET['p']))
{
$page = $_GET['p'];

switch($page){
	
	case 'accueil': include("accueil.php"); break;
	
	case 'presentation': include("presentation.php"); break;
	
	case 'hebergement': include("hebergements/hebergement.php"); break;
	
	case 'chambre-double': include("hebergements/chambre_double.php"); break;
	case 'chambre-single': include("hebergements/chambre_single.php"); break;
	case 'suite-presidentielle': include("hebergements/suite_presidentielle.php"); break;

    case 'evenements': include("evenements/evenements.php"); break;
	case 'events': include("evenements/event.php"); break;
	
	case 'espaces': include("espaces/espace.php"); break;
	case 'piscine': include("espaces/piscine.php"); break;

	case 'promotions': include("promotions/promotions.php"); break;
	case 'promos': include_once("promotions/promo.php"); break;
	
	case 'embekieman': include("restaurants/embekieman.php"); break;
	case 'allocodrome': include("restaurants/yankady.php"); break;
	
	case 'barika': include("bars/barika.php"); break;
	case 'nightclub': include("bars/nightclub.php"); break;
	
	case 'salle': include("salles/salle.php"); break;
	
	case 'video': include("videos/video.php"); break;
	
	case 'galerie': include("photos/galerie.php"); break;
	case 'photos': include("photos/"); break;
	
	case 'contacts': include("contacts.php"); break;
	case 'reservation': include("reservation.php"); break;
	case 'reserver': include("reserver.php"); break;
	case 'consulter': include("consulter.php"); break;
	case 'modifier-ma-reservation': include("edit_reservation.php"); break;
	case 'modifier': include("modifier.php"); break;
	
	case '404': include("404.php"); break;
	
	default: include("404.php"); break;	
	}
}
else
{
	include("accueil.php");
	$page='accueil';
}
?>
  
<?php
echo "<script>
$(function(){	
	$(\"#menu li a[id='$page']\").addClass(\"activegray\");	
	$(\".listWithMarker li a[id='$page']\").addClass(\"activegray\");	
});
	</script>";		

?>  

<?php include('leftbox/script.php'); ?>

     
</div>

<!--FORMULAIRE DE CONSULATATION DES RESERVATIONS-->
<div style="display:none">
	<form id="consult_form" method="post" action="?p=consulter">
    <h2>Consulter votre réservation </h2>
    <p id="login_error"><em>Veuillez remplir tous les champs</em></p>
		<p>
			<label for="login_name">Code de réservation: </label><br>
			<input type="text" id="login_code" name="code" size="10" required />
		</p>
		<p>
			<label for="login_pass">Nom de famille : </label><br>
			<input type="text" id="login_name" name="nom" size="30" required />
		</p>
		<p>
		  <input type="submit" value="CONSULTER" />
		</p>
		<p>
		    
	  </p>
  </form>
</div>


<!-- coded by Nehemie KOFFI -->


<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://pgc.ci/piwik/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "3"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->

</body>
</html>